var lines = [
    'Good things come to those who wait, but woe to those who wait until it is too late to receive them.',
    'It is the bane of ambition and productivity. The sole reason why work is never complete, or even never started. How do I know this? Well, I have a few numbers to back me up.',
    'Admittedly, those numbers aren\'t very solid, statistically speaking. But then again, they don\'t have to be, for this was merely a trick, designed to amuse you. Though you may not be amused, but instead irritated that this is only here to waste your time and mine.',
    'Sometimes, that\'s the point. To do or to make something that is, in the end, pointless. It\'s a way to test your skills, to try something new, or even just to pass the time for a while--some would say \'killing time\'.',
    'That isn\'t to say it\'s a good thing to do all the time, as you may discover after realizing you were up until 3 AM playing games for nearly 10 hours. On the flipside, never taking the time for yourself is just as bad; as Stephen King once wrote, \"All work and no play makes Jack a dull boy.\"',
    'We all carry grand plans of one kind or another. Whether it for school projects, fantastic vacations, or that bestseller you\'ve always wanted to write, we collect ideas from a variety of places. They come from the various media we consume--books, movies, games, and the like--and from the people we interact with, online and off.',
    'All too often, though, the only thing we do is collect ideas. Or plan things. Endlessly. We become enamored with the developing the concepts in our heads to the point where we never have the time to set them in motion.Or perhaps we get too scared to even try, terrified that what we\'re working on won\'t ever shape up to what we\'ve thought of it.',
    'In that sense, it can be understandable why we procrastinate, why we put off work or other things in favor of something more enjoyable. Or maybe instead we find ourselves not in the right mood to work, and needsomething to help you \"get in the zone\" as some would put it. Many of those means also happen to be where we get our ideas, too.',
    'Admittedly, it\'s been some time since I read a book for leisure. Never mind how much of a time commitment they can be,the nature of society and the internet facilitates bite-sized forms of entertainment, which can be easily consumed on breaks between any sort of work.',
    'Never really been a movie person, and my TV watching has dropped off in recent years. Thanks internet. Not to say I dislike either, per se. I just rarely have the opportunity to watch, or use it as background noise.',
    'Speaking of the internet, sites like YouTube  and Twitch have evolved into major time sinks and sources of my entertainment.',
    'From the amusing to the informative to the slightly bizarre, you can find a variety of channels to suit your needs and interests.',
    'Tasked with a delivery of the utmost importance, journey across a cursed land in \"The Messenger\"',
    'The other major source of entertainment today would have to be games. From platformers to puzzlers to the oft maligned shooters, there\'s no doubt that thousands of hours have been spent whiling away at one escapade or another.',
    'I aspire to make a game of my own, and have even built a short game of rock-paper-scissors previously, though it is admittedly basic in nature.',
    'Spending time with friends has become more important as I have grown older. Sometimes this means getting out of the house/dorm to meet with people, for such things as D&D or local fighting game tournaments.',
    'Most of my interactions, however, are online, by means of platforms like Discord, a free online text and voice communications service. It is also a place where I have made many an acquatence with people out of state, even out of country.',
    'There are other places to chat with friends and others, Twitter and tumblr among them. They also serve as a means to view and share the works of countless talented artists from across the world.',
    'A selection of art I enjoy--click to view the full picture in a new tab.'
]

console.log($('p'));
var data = [
    {slot: 0, value: 15},{slot: 1, value: 10},{slot: 2, value: 10},{slot: 3, value: 5},{slot: 4, value: 2},
    {slot: 5, value: 8},{slot: 6, value: 13},{slot: 7, value: 3},{slot: 8, value: 14},{slot: 9, value: 1},
    {slot: 10, value: 12},{slot: 11, value: 6},{slot: 12, value: 1},{slot: 13, value: 19},{slot: 14, value: 17},
    {slot: 15, value: 6},{slot: 16, value: 15},{slot: 17, value: 5},{slot: 18, value: 3},{slot: 19, value: 15},
    {slot: 20, value: 5},{slot: 21, value: 12},{slot: 22, value: 17},{slot: 23, value: 19},{slot: 24, value: 10}
]
function updateParagraphText() {
    var pId = "";
  for(var i = 0; i < $('p').length; i++){
      pId = "#line" + i;
      $(pId).text(lines[i]);
  }
}
updateParagraphText();
var graphHidden = true;
var state = 0;
var str="";

var graph =  Morris.Line({
    // ID of the element in which to draw the chart.
    element: "joke-graph",
    // Chart data records -- each entry in this array corresponds to a point on the chart.
    data: data,
    // The name of the data record attribute that contains x-values.
    xkey: "slot",
    // A list of names of data record attributes that contain y-values.
    ykeys: ["value"],
    // Labels for the ykeys -- will be displayed when you hover over the chart.
    labels: ["Value"],
    redraw: true,
    ymin: 0,
    ymax: 21,
    hideHover: 'always',
    fillOpacity: 0.7,
    parseTime: false
});

$('.btn-info').click(function(){
    $('#joke-graph').css('visibility','visible');
    $(this).css('visibility','hidden');
    $('#part2').css('visibility','visible');
    graphHidden = false
    state++;
    $('.container').append("<br><div class=\'btn btn-primary btn-lg btn-block\'>Continue</div><br>");
});

$('.container').on('click','.btn-primary',function(){
    var appendSTR = "";
    switch(state){
    case 1: 
    appendSTR += "<h3 id=\'ambition\'>AMBITION</h3><hr><div class=\"container-fluid\"><p id=\"line5\"></p>"
        +"<div class=\"row\"><div class=\"col\"><p id=\"line6\"></p><p id=\"line7\"></p></div>"
        //Artist unknown
        +"<div class=\"col-7\" id=\'carried-away-image\'><img src=\'./image/carriedAway.jpg\' alt=\'Carried Away\'title=\'Artist unknown\'></div></div></div>"
        +"<br><div class=\'btn btn-primary btn-lg btn-block\'>Continue</div><br>";
        $('.btn.ambition').css('visibility','visible').removeClass('btn-disabled').addClass('btn-secondary').text('Ambition');
        break;
    case 2: 
        appendSTR += "<h3 id=\'books\'>BOOKS</h3><hr><div class=\"container-fluid\"><div class=\"row\">"
        +"<div class=\'col-8\'><p id=\"line8\"></p></div><div class=\'col\'><img src=\"\" alt=\'Books I\'ve read\'></div></div></div>"
        +"<br><div class=\'btn btn-primary btn-lg btn-block\'>Continue</div><br>";
        $('.btn.books').css('visibility','visible').removeClass('btn-disabled').addClass('btn-secondary').text('Books');
        break;
    case 3: 
        appendSTR += "<h3 id=\'movies\'>MOVIES AND TELEVISION</h3><hr><div class=\"container-fluid\"><div class=\"row\">"
        +"<div class=\'col-5\'><img src=\"\" alt=\'Movies/TV I\'ve watched\'></div><div class=\'col\'><p id=\"line9\"></p></div></div></div>"
        +"<br><div class=\'btn btn-primary btn-lg btn-block\'>Continue</div><br>";
        $('.btn.movies').css('visibility','visible').removeClass('btn-disabled').addClass('btn-secondary').text('Movies/Television');
        break;
    case 4: 
        appendSTR += "<h3 id=\'internet\'>INTERNET VIDEOS</h3><hr><div class=\"container-fluid\"><div class=\"row\">"
        +"<div class=\'col\'><p id=\"line10\"></p><p id=\"line11\"></p></div><div class=\'col\'>"
        //From Wikimedia commons
        +"<a href=\'https://www.youtube.com/\' target=\'_blank\' rel=\'noreferrer noopener\'><img src=\'./image/1280px-YouTube_full-color_icon_(2017).svg.png\' width=\'640\' height=\'450\'></a></div></div></div>"
        +"<br><div class=\'btn btn-primary btn-lg btn-block\'>Continue</div><br>";
        $('.btn.internet').css('visibility','visible').removeClass('btn-disabled').addClass('btn-secondary').text('Internet');
        break;
    case 5: 
        appendSTR += "<h3 id=\'gaming\'>GAMING</h3><hr><div class=\"container-fluid\"><div class=\"row\">"
        +"<div class=\'col-4\'><div class=\'card\'><div class=\'card-top\'><a href=\'https://store.steampowered.com/app/764790/The_Messenger/\' target=\'_blank\' rel=\'roreferrer noopener\'>"
        +"<img src=\'./image/EGS_Sabotage_TheMessenger.jpg\' alt=\'The Messenger by Sabotage Studio\' title=\'The Messenger by Sabotage Studio\' height=\'180px\' width=\'320px\'></a></div>"
        +"<div class=\'card-body\'><p id=\'line12\' class=\'pic-desc\'></p></div></div></div><div class=\'col\'><p id=\"line13\"></p><p id=\"line14\"></p></div></div></div>"
        +"<br><div class=\'btn btn-primary btn-lg btn-block\'>Continue</div><br>";
        $('.btn.games').css('visibility','visible').removeClass('btn-disabled').addClass('btn-secondary').text('Gaming');
        break;
    case 6: 
        appendSTR += "<h3 id=\'social\'>SOCIAL MEDIA</h3><hr><div class=\"container-fluid\"><div class=\"row\">"
        //From wikimedia commons
        +"<div class=\'col\'><img src=\'./image/778px-Discord_color_D.svg.png\' title=\'Discord\' alt=\'Colored D\' width=\'389\' height=\'512\'></div><div class=\'col-7\'><p id=\'line15\'></p><p id=\'line16\'></p></div></div></div>"
        +"<br><div class=\'btn btn-primary btn-lg btn-block\'>Continue</div><br>";
        $('.btn.connections').css('visibility','visible').removeClass('btn-disabled').addClass('btn-secondary').text('Social Media');
        break;
    case 7: 
        appendSTR += "<h3 id=\'art\'>ART</h3><hr><div class=\"container-fluid\"><div class=\"row\">"
        + "<div class=\'col-4\'><p id=\'line17\'></div><div class=\'col\'><div class=\"row\">" //It was around this time that I started losing interest in putting the site together
        //https://twitter.com/kei2468
        +"<a href=\'./image/kirby_inkling_painters.jpg\' target=\'_blank\' rel=\'noreferrer noopener\'><img src=\'./image/kirby_inkling_painters.jpg\' class=\'art-display\' alt=\'Paint-Kirby-and-Inkling-Girl\' title=\'by kei2468 on Twitter\'></a>"
        //https://www.deviantart.com/iplatartz/art/FBI-eevee-793679303
        +"<a href=\'./image/eevee_fbi.jpg\' target=\'_blank\' rel=\'noreferrer noopener\'><img src=\'./image/eevee_fbi.jpg\' class=\'art-display\' alt=\'Eevee from the FBI\' title=\'by IPlatArtz on DeviantArt\'></a>"
        //https://staticsrecyclebin.tumblr.com/post/170120052131/square-up-thot
        +"<a href=\'./image/chibiBuffsuki.png\' target=\'_blank\' rel=\'noreferrer noopener\'><img src=\'./image/chibiBuffsuki.png\' class=\'art-display\' alt=\'Square Up\' title=\'by staticsrecyclebin on tumblr\'></a>"
        //https://www.deviantart.com/phiphiauthon/art/JotaLyn-778914440
        +"<a href=\'./image/lyndis_jojoCosplay.jpg\' target=\'_blank\' rel=\'noreferrer noopener\'><img src=\'./image/lyndis_jojoCosplay.jpg\' class=\'art-display\' alt=\'JotaLyn\' title=\'by PhiphiAuThon on DeviantArt\'></a>"
        //https://www.deviantart.com/the-emerald-otter/art/Pokemon-Fusion-Cosmicott-722654545
        +"<a href=\'./image/cosmog_whimsicott_fusion.png\' target=\'_blank\' rel=\'noreferrer noopener\'><img src=\'./image/cosmog_whimsicott_fusion.png\' class=\'art-display\' alt=\'Cosmicott\' title=\'by The-Emerald-Otter on DeviantArt\'></a>"
        //https://twitter.com/edgycat/status/1142588596748242946?lang=en
        +"<a href=\'./image/nessNESwNessa_Tessie_LochNess.jpg\' target=\'blank\' rel=\'noreferrer noopener\'><img src=\'./image/nessNESwNessa_Tessie_LochNess.jpg\' class=\'art-display\' alt=\'Ness and Nessa with an NES on Tessie in Loch Ness\' title=\'by EdgyCat on Twitter\'></a></div>"
        +"<div class=\"row\"><p id=\'line18\' class=\'pic-desc\'></p></div></div></div></div>"
        + "<hr><a href=\"#top\">Return to top.</a>";
        $('.btn.art').css('visibility','visible').removeClass('btn-disabled').addClass('btn-secondary').text('Art');
        break;
    default: break;
    }
    $(this).css('visibility','hidden');
    $('.container').append(appendSTR);
    updateParagraphText();
    state++;
  });

  function updateGraph(){
    if(!graphHidden){
        var nxVal;
        for(var i = 0; i < data.length;i++){
            nxVal = Math.floor(Math.random() * 20 + 1);
            data[i].value = nxVal
        }
        graph.setData(data);
    }
  }
  setInterval(updateGraph,5000);