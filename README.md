# README #

This README details some of the design of this website.

### What is this repository for? ###

* This repository was created to act as a demonstration of the skills and concepts learned in an Introduction to Web Media Design class. These 
  concepts include CSS styling, use of JavaScript and jQuery, and utilization of external libraries and frameworks in the site design. Additionally, 
  this site is also intended to discuss a topic of my choosing, and also showcase some of the various media and platforms I draw my inspiration from.
  
* The site is comprised of several differenct subsections, each with an accompanying text block and graphic. These accompanying graphics consist of
  either a single or set of images or an object constructed using JavaScript. Certain subsections may also include an action, such as revealing or 
  changing some element within the subsection.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact